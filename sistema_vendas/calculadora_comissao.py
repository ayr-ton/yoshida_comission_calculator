import math

class CalculadoraComissao(object):

    @classmethod
    def calcular(cls, valor_venda):
        if valor_venda > 10000:
            comissao = valor_venda * 6 / 100
        else:
            comissao = valor_venda * 5 / 100

        return math.floor(comissao * 100) / 100
from flask import Flask, escape, request

from .calculadora_comissao import CalculadoraComissao

app = Flask(__name__)

@app.route('/comissao/<float:venda>')
def comissao(venda):
    return '{:.2f}'.format(CalculadoraComissao.calcular(venda))

@app.route('/health')
def health():
    return {"status": "OK"}, 200

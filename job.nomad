job "[[.name]]" {
  datacenters = ["dc1"]
  type = "service"
  
  group "calculator" {
    count = 3
    update {
      max_parallel = 6
    }
    
    task "api" {
      driver = "docker"
      
      config {
        image = "[[.image]]"
        port_map {
          http = 8000
        }
      }

      resources {
        network {
          mbits = 10
          port "http" {}
        }
      }

      service {
        name = "flask-api"
        port = "http"
        tags = ["urlprefix-[[.name]].[[.domain]]/"]
        check {
          name     = "alive"
          type     = "http"
          path     = "/health"
          interval = "10s"
          timeout  = "3s"
        }
      }
    }
  }
}
